/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package softdecoffee.pos;

import softdecoffee.model.Product;

/**
 *
 * @author Admin
 */
public interface BuyProductable {
    public void buy(Product product, int qty);
}
