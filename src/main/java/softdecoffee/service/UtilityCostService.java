/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.UtilityCostDao;
import softdecoffee.model.UtilityCost;

/**
 *
 * @author Nobpharat
 */
public class UtilityCostService {

	public UtilityCost getById(int id) {
		UtilityCostDao utilityCostDao = new UtilityCostDao();
		return  utilityCostDao.get(id);
	}

	public List<UtilityCost> getUtilityCosts() {
		UtilityCostDao  utilityCostDao = new UtilityCostDao();
		return  utilityCostDao.getAll("UC_CODE asc");
	}
	
	public List<UtilityCost> getUtilityCostsByDate(String date) {
		UtilityCostDao  utilityCostDao = new UtilityCostDao();
		return  utilityCostDao.getAll(date, "UC_CODE asc");
	}


	public UtilityCost addNew(UtilityCost editedUtilityCost) {
		UtilityCostDao  utilityCostDao = new UtilityCostDao();
		return  utilityCostDao.save(editedUtilityCost);
	}

	public UtilityCost update(UtilityCost editedUtilityCost) {
		UtilityCostDao  utilityCostDao = new UtilityCostDao();
		return  utilityCostDao.update(editedUtilityCost);
	}

	public int delete(UtilityCost editedUtilityCost) {
		UtilityCostDao  utilityCostDao = new UtilityCostDao();
		return  utilityCostDao.delete(editedUtilityCost);
	}
}
