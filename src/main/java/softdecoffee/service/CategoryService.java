/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.CategoryDao;
import softdecoffee.model.Category;

/**
 *
 * @author basba
 */
public class CategoryService {
    public Category getById(int id){
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.get(id);
    }
    
    public List<Category> getCategorys(){
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll("CAT_CODE asc");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    }
}
