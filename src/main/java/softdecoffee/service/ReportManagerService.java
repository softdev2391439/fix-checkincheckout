/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.ReportManagerDao;
import softdecoffee.model.ReportManager;

/**
 *
 * @author Nobpharat
 */
public class ReportManagerService {

	public List<ReportManager> getBestSell(String date, int limit) {
		ReportManagerDao productDao = new ReportManagerDao();
		return productDao.getBestSellByDate(date, limit);
	}
	public List<ReportManager> getMoneyInfo(String start, String end) {
		ReportManagerDao productDao = new ReportManagerDao();
		return productDao.getMoneyInfo(start, end);
	}
	public List<ReportManager> getBalanceMaterial() {
		ReportManagerDao productDao = new ReportManagerDao();
		return productDao.getBalanceMaterial();
	}
	public List<ReportManager> getMoneyInfoDESC(String start, String end) {
		ReportManagerDao productDao = new ReportManagerDao();
		return productDao.getMoneyInfoDESC(start, end);
	}
}
