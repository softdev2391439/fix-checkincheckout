package softdecoffee.service;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import java.util.List;
import softdecoffee.dao.ReceiptDao;
import softdecoffee.dao.ReceiptDetailDao;
import softdecoffee.model.Receipt;
import softdecoffee.model.ReceiptDetail;

/**
 * Service class for managing Receipt objects.
 */
public class ReceiptService {

      public Receipt getById(int id) {
                ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.get(id);
    }
    
    public List<Receipt> getReceipts(){
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll(" RE_CODE asc ");
    }
    
    public String getProduceNameByRcdCode(int id) {      
        return ReceiptDetailDao.getProductNameByProductCode(1);
    }

   public Receipt addNew(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        Receipt receipt = receiptDao.save(editedReceipt);
        for(ReceiptDetail rd: editedReceipt.getReceiptDetails()){
            rd.setReceiptId(receipt.getId());
            receiptDetailDao.save(rd);
        }
        return receipt;
    }

    public Receipt update(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.update(editedReceipt);
    }

    public int delete(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.delete(editedReceipt);
    }

}