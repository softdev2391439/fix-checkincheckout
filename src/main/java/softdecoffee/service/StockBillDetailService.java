/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.StockBillDetailDao;
import softdecoffee.model.StockBillDetail;



/**
 *
 * @author Nobpharat
 */
public class StockBillDetailService {
public StockBillDetail getById(int id){
        StockBillDetailDao stockBillDetailDao  = new StockBillDetailDao();       
        return stockBillDetailDao.get(id);
    }
    
    public List<StockBillDetail>getByBillID(int id){
        StockBillDetailDao stockBillDetailDao = new StockBillDetailDao();
        return stockBillDetailDao.getByCheckId(id);
        
    }
    
    public List<StockBillDetail> getBillStocks(){
        StockBillDetailDao stockBillDetailDao = new StockBillDetailDao();
        return stockBillDetailDao.getAll(" BD_CODE asc");
    }
    
    
    public StockBillDetail addNew(StockBillDetail editedStockBillDetail) {
        StockBillDetailDao stockBillDetailDao = new StockBillDetailDao();
        return stockBillDetailDao.save(editedStockBillDetail);
    }

    public StockBillDetail update(StockBillDetail editedStockBillDetail) {
        StockBillDetailDao stockBillDetailDao = new StockBillDetailDao();
        return stockBillDetailDao.update(editedStockBillDetail);
    }

    public int delete(StockBillDetail editedStockBillDetail) {
        StockBillDetailDao stockBillDetailDao = new StockBillDetailDao();
        return stockBillDetailDao.delete(editedStockBillDetail);
    }	
}
