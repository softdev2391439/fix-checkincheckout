/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.StockBillDao;
import softdecoffee.dao.VendorDao;
import softdecoffee.model.StockBill;
import softdecoffee.model.Vendor;

/**
 *
 * @author Puri
 */
public class StockBillService {

//    public StockBill getByVen(int id) {
//        StockBillDao stockBillDao = new StockBillDao();
//        StockBill stockBill = stockBillDao.getByVendor(id);
//        return stockBill;
//    }
    public StockBill getById(int id) {
        StockBillDao stockBillDao = new StockBillDao();
        return stockBillDao.get(id);
    }

//    public ArrayList<StockBill> getAll() {
//        StockBillDao stockBillDao = new StockBillDao();
//        return (ArrayList<StockBill>) stockBillDao.getAll();
//    }
    public List<StockBill> getAll() {
        StockBillDao stockBillDao = new StockBillDao();
        return stockBillDao.getAll();
    }

    public List<StockBill> getStockBills() {
        StockBillDao stockBillDao = new StockBillDao();
        return stockBillDao.getAll(" BILL_CODE asc");
    }

//    public List<StockBill> getVendorId(String where, String order){
//        StockBillDao stockBillDao = new StockBillDao();
//        return stockBillDao.getAll(where, order);
//    }
//    public List<StockBill> getByVendor(int vendorId) {
//        StockBillDao stockBillDao = new StockBillDao();
//        // Assuming you have a suitable where condition to filter by vendorId
//        String whereCondition = "V_CODE = " + vendorId;
//        return stockBillDao.getAll(whereCondition);
//    }
    public List<StockBill> getByVendor(int vendorId) {
        StockBillDao stockBillDao = new StockBillDao();
        return stockBillDao.getByVendor(vendorId);
    }

    public StockBill addNew(StockBill editedStockBill) {
        StockBillDao stockBillDao = new StockBillDao();
        return stockBillDao.save(editedStockBill);
    }

    public StockBill update(StockBill editedStockBill) {
        StockBillDao stockBillDao = new StockBillDao();
        return stockBillDao.update(editedStockBill);
    }

    public int delete(StockBill editedStockBill) {
        StockBillDao stockBillDao = new StockBillDao();
        return stockBillDao.delete(editedStockBill);
    }
}
