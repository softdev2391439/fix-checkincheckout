/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.PayrollDao;
import softdecoffee.model.Payroll;

/**
 *
 * @author werapan
 */
public class PayrollService {

    public Payroll getById(int id) {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.get(id);
    }

    public List<Payroll> getPayrolls() {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.getAll(" PRL_CODE asc");
    }

    public List<Payroll> getByStatus() {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.getByStatus();
    }

    public Payroll addNew(Payroll editedPayroll) {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.save(editedPayroll);
    }

    public Payroll update(Payroll editedPayroll) {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.update(editedPayroll);
    }

    public int delete(Payroll editedPayroll) {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.delete(editedPayroll);
    }
}
