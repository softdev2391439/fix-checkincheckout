/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import softdecoffee.model.Vendor;
import softdecoffee.service.VendorService;

/**
 *
 * @author Puri
 */
public class VendorTestService {
    public static void main(String[] args) {
        VendorService vs = new VendorService();
        for(Vendor vendor :vs.getVendors()){
            System.out.println(vendor);
        }
        //System.out.println(vs.getById(1));
        System.out.println("-----------");
        Vendor ven1 = new Vendor(5,"TH","chonburi town","081-111-1111","PP Local market","PPLocalMarket@gmail.com","department store");
        vs.addNew(ven1);
        //vs.delete(ven1);
        for(Vendor vendor :vs.getVendors()){
            System.out.println(vendor);
        }
        
    }
}
