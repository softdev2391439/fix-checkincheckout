/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import softdecoffee.model.UtilityCost;
import softdecoffee.service.UtilityCostService;

/**
 *
 * @author Nobpharat
 */
public class UtilityCostTest {

	public static void main(String[] args) {
		UtilityCostService costService = new UtilityCostService();
		LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formattedDateTime = localDateTime.format(formatter);
		System.out.println(formattedDateTime);
		  UtilityCost newCost = new UtilityCost(70, 80, 90, 240, formattedDateTime);
		  costService.addNew(newCost);
		  UtilityCost upCost = costService.getById(1);
		  upCost.setDateString("2023-09-23 12:55:00");
		  costService.update(upCost);
		for (UtilityCost cost : costService.getUtilityCosts()) {
			System.out.println(cost);
		}

	}
}
