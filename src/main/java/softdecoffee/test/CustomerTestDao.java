/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import softdecoffee.dao.CustomerDao;
import softdecoffee.model.Customer;

/**
 *
 * @author natta
 */
public class CustomerTestDao {

    public static void main(String[] args) {
        CustomerDao cus = new CustomerDao();

        //Customer c1 = new Customer("Doldy", "Gaming", "Doldy@gmail.com", "091-256-1587", "Doldy road", "Female");
        //cus.save(c1);
        
        //for (Customer customer : cus.getAll()) {
        //    System.out.println(customer);
        //}
        
        
        Customer c4 = cus.get(1);
        cus.delete(c4);
        Customer c2 = cus.get(3);
        c2.setFname("Ambussing");
       cus.update(c2);
       for (Customer customer : cus.getAll()) {
           System.out.println(customer);
        }
        
        
    }
}
