/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import softdecoffee.model.ReportManager;
import softdecoffee.service.ReportManagerService;

public class ReportManagerTest {

    public static void main(String[] args) {
        // testBestSeller
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = localDate.format(formatter);
        System.out.println("Formatted Date: " + formattedDate);
        String mockDate = "2023-10-09";
        List<ReportManager> products = new ReportManagerService().getBestSell(mockDate, 5);

        for (ReportManager p : products) {
            System.out.println(p);
        }

        // Money
      String startMock = "2020-01-01";
       String endMock = "2023-12-31";
        List<ReportManager> moneys = new ReportManagerService().getMoneyInfo((startMock), (endMock));
        for (ReportManager m : moneys) {
            System.out.println(m);
        }

        // Material
        ReportManagerService materials = new ReportManagerService();
        for (ReportManager mat : materials.getBalanceMaterial()) {
            System.out.println(mat);
        }
    }

}
