/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import softdecoffee.dao.EmployeeDao;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.Customer;
import softdecoffee.model.Employee;

/**
 *
 * @author Admin
 */
public class TestEmployeeDao {
    public static void main(String[] args) {
        
        
        EmployeeDao emd = new EmployeeDao();
        Employee em = emd.get(1);
        em.setTel("111-111-1111");
        emd.update(em);
        System.out.println(em);
        /*for (Employee em : emd.update(obj)) {
            System.out.println(em); // Print each employee or their details here
        }*/
        DatabaseHelper.close();
    }
}
