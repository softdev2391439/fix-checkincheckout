/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.TopTenSalesReport;

/**
 *
 * @author natta
 */
public class TopTenSalesDao implements Dao {

    @Override
    public Object get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object save(Object obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object update(Object obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<TopTenSalesReport> getArtistByTotalPrice(int limit) {
        ArrayList<TopTenSalesReport> list = new ArrayList();
        String sql;
        sql = """
              SELECT Pro.PROD_CODE,Pro.PROD_NAME, SUM(Rcd.RCD_QTY) TotalQuantity,
                                   SUM(Pro.PROD_PRICE*Rcd.RCD_QTY) as  TotalPrice FROM PRODUCT Pro
                                      INNER JOIN RECEIPT_DETAIL Rcd ON Rcd.PROD_CODE=Pro.PROD_CODE
                                      INNER JOIN RECEIPT Rc ON Rc.RE_CODE=Rcd.RC_CODE
                                       AND Rc.RE_DATE ="2023-10-09 08:15:00"
                                        GROUP BY Pro.PROD_CODE
                                        ORDER BY TotalPrice DESC
                                        LIMIT ?
              """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                TopTenSalesReport obj = TopTenSalesReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    
     public List<TopTenSalesReport> getArtistByTotalPrice(String begin,String end,int limit) {
        ArrayList<TopTenSalesReport> list = new ArrayList();
        String sql = """
                     SELECT Pro.PROD_CODE,Pro.PROD_NAME, SUM(Rcd.RCD_QTY) TotalQuantity,
                                          SUM(Pro.PROD_PRICE*Rcd.RCD_QTY) as  TotalPrice FROM PRODUCT Pro
                                             INNER JOIN RECEIPT_DETAIL Rcd ON Rcd.PROD_CODE=Pro.PROD_CODE
                                             INNER JOIN RECEIPT Rc ON Rc.RE_CODE=Rcd.RC_CODE   
                                              AND Rc.RE_DATE BETWEEN ? AND ? 
                                               GROUP BY Pro.PROD_CODE
                                               ORDER BY TotalPrice DESC
                                               LIMIT ?
                     """;

        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                TopTenSalesReport obj = TopTenSalesReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
}
