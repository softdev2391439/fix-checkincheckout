/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.ReportManager;

/**
 *
 * @author Nobpharat
 */
public class ReportManagerDao implements Dao<ReportManager> {

	public ReportManager get(int id) {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}

	public List<ReportManager> getAll() {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}

	public ReportManager save(ReportManager obj) {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}

	public ReportManager update(ReportManager obj) {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}

	public int delete(ReportManager obj) {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}

	/**
	 *
	 * @param where
	 * @param order
	 * @return
	 */
	public List<ReportManager> getAll(String where, String order) {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}

	public List<ReportManager> getBestSellByDate(String date, int limit) {
		List<ReportManager> list = new ArrayList<>();
		String sql = "SELECT p.PROD_CODE AS P_ID, p.PROD_NAME AS P_NAME, SUM(rd.RCD_QTY) AS P_TOTALQTY "
			+ "FROM PRODUCT p "
			+ "JOIN RECEIPT_DETAIL rd ON p.PROD_CODE = rd.PROD_CODE "
			+ "JOIN RECEIPT r ON r.RE_CODE = rd.RC_CODE "
			+ "WHERE strftime('%m', r.RE_DATE) = strftime('%m', ?) "
			+ "GROUP BY p.PROD_CODE "
			+ "ORDER BY P_TOTALQTY DESC "
			+ "LIMIT ?";
		Connection conn = DatabaseHelper.getConnect(); 
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setString(1, date);
			stmt.setInt(2, limit);
			try (ResultSet rs = stmt.executeQuery()) {
				while (rs.next()) {
					ReportManager obj = ReportManager.fromRSBestSale(rs);
					list.add(obj);
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public List<ReportManager> getMoneyInfo(String start, String end) {
		List<ReportManager> list = new ArrayList<>();
		String sql = "SELECT Date, SUM(ELEC_PAY) AS ELEC_PAY, SUM(WATER_PAY) AS WATER_PAY, "
			+ "SUM(RENT_PAY) AS RENT_PAY, SUM(EMP_PAY) AS EMP_PAY, SUM(MAT_PAY) AS MAT_PAY, "
			+ "SUM(UC_PAY) AS UC_PAY, SUM(TOTAL_INCOME) AS TOTAL_INCOME, SUM(TOTAL_COST) AS TOTAL_COST, "
			+ "SUM(TOTAL_INCOME) - SUM(TOTAL_COST) AS TOTAL_PROFIT FROM (SELECT DATE(UC_DATE) AS Date, "
			+ "SUM(UC_ELECTRIC) AS ELEC_PAY, SUM(UC_WATER) AS WATER_PAY, SUM(UC_RENTAL) AS RENT_PAY, "
			+ "SUM(UC_TOTAL) AS UC_PAY, 0 AS EMP_PAY, 0 AS MAT_PAY, 0 AS TOTAL_INCOME, "
			+ "SUM(UC_ELECTRIC + UC_WATER + UC_RENTAL) AS TOTAL_COST FROM UTILITY_COST GROUP BY DATE(UC_DATE) "
			+ "UNION ALL SELECT DATE(PRL_DATE) AS Date, 0 AS ELEC_PAY, 0 AS WATER_PAY, 0 AS RENT_PAY, "
			+ "0 AS UC_PAY, SUM(PRL_TOTAL_PAY) AS EMP_PAY, 0 AS MAT_PAY, 0 AS TOTAL_INCOME, "
			+ "SUM(PRL_TOTAL_PAY) AS TOTAL_COST FROM PAYROLL GROUP BY DATE(PRL_DATE) "
			+ "UNION ALL SELECT DATE(BILL_DATETIME) AS Date, 0 AS ELEC_PAY, 0 AS WATER_PAY, 0 AS RENT_PAY, "
			+ "0 AS EMP_PAY, 0 AS UC_PAY, SUM(BILL_PAY) AS MAT_PAY, 0 AS TOTAL_INCOME, "
			+ "SUM(BILL_PAY) AS TOTAL_COST FROM BILL_STOCK GROUP BY DATE(BILL_DATETIME) "
			+ "UNION ALL SELECT DATE(RE_DATE) AS Date, 0 AS ELEC_PAY, 0 AS WATER_PAY, 0 AS RENT_PAY, "
			+ "0 AS EMP_PAY, 0 AS MAT_PAY, 0 AS UC_PAY, SUM(RE_NET_TOTAL) AS TOTAL_INCOME, 0 AS TOTAL_COST "
			+ "FROM RECEIPT GROUP BY DATE(RE_DATE)) AS CombinedData WHERE Date >= ? "
			+ "AND Date <= ? GROUP BY Date;";
		Connection conn = DatabaseHelper.getConnect(); 
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setString(1, start);
			stmt.setString(2, end);
			try (ResultSet rs = stmt.executeQuery()) {
				while (rs.next()) {
					ReportManager obj = ReportManager.fromRSMoney(rs);
					list.add(obj);
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}
	public List<ReportManager> getMoneyInfoDESC(String start, String end) {
		List<ReportManager> list = new ArrayList<>();
		String sql = "SELECT Date, SUM(ELEC_PAY) AS ELEC_PAY, SUM(WATER_PAY) AS WATER_PAY, "
			+ "SUM(RENT_PAY) AS RENT_PAY, SUM(EMP_PAY) AS EMP_PAY, SUM(MAT_PAY) AS MAT_PAY, "
			+ "SUM(UC_PAY) AS UC_PAY, SUM(TOTAL_INCOME) AS TOTAL_INCOME, SUM(TOTAL_COST) AS TOTAL_COST, "
			+ "SUM(TOTAL_INCOME) - SUM(TOTAL_COST) AS TOTAL_PROFIT FROM (SELECT DATE(UC_DATE) AS Date, "
			+ "SUM(UC_ELECTRIC) AS ELEC_PAY, SUM(UC_WATER) AS WATER_PAY, SUM(UC_RENTAL) AS RENT_PAY, "
			+ "SUM(UC_TOTAL) AS UC_PAY, 0 AS EMP_PAY, 0 AS MAT_PAY, 0 AS TOTAL_INCOME, "
			+ "SUM(UC_ELECTRIC + UC_WATER + UC_RENTAL) AS TOTAL_COST FROM UTILITY_COST GROUP BY DATE(UC_DATE) "
			+ "UNION ALL SELECT DATE(PRL_DATE) AS Date, 0 AS ELEC_PAY, 0 AS WATER_PAY, 0 AS RENT_PAY, "
			+ "0 AS UC_PAY, SUM(PRL_TOTAL_PAY) AS EMP_PAY, 0 AS MAT_PAY, 0 AS TOTAL_INCOME, "
			+ "SUM(PRL_TOTAL_PAY) AS TOTAL_COST FROM PAYROLL GROUP BY DATE(PRL_DATE) "
			+ "UNION ALL SELECT DATE(BILL_DATETIME) AS Date, 0 AS ELEC_PAY, 0 AS WATER_PAY, 0 AS RENT_PAY, "
			+ "0 AS EMP_PAY, 0 AS UC_PAY, SUM(BILL_PAY) AS MAT_PAY, 0 AS TOTAL_INCOME, "
			+ "SUM(BILL_PAY) AS TOTAL_COST FROM BILL_STOCK GROUP BY DATE(BILL_DATETIME) "
			+ "UNION ALL SELECT DATE(RE_DATE) AS Date, 0 AS ELEC_PAY, 0 AS WATER_PAY, 0 AS RENT_PAY, "
			+ "0 AS EMP_PAY, 0 AS MAT_PAY, 0 AS UC_PAY, SUM(RE_NET_TOTAL) AS TOTAL_INCOME, 0 AS TOTAL_COST "
			+ "FROM RECEIPT GROUP BY DATE(RE_DATE)) AS CombinedData WHERE Date >= ? "
			+ "AND Date <= ? GROUP BY Date  ORDER BY Date DESC;";
			
		Connection conn = DatabaseHelper.getConnect(); 
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setString(1, start);
			stmt.setString(2, end);
			try (ResultSet rs = stmt.executeQuery()) {
				while (rs.next()) {
					ReportManager obj = ReportManager.fromRSMoney(rs);
					list.add(obj);
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public List<ReportManager> getBalanceMaterial() {
		List<ReportManager> list = new ArrayList<>();
		String sql = "SELECT M_CODE AS M_ID, M_NAME, (M_QOH - M_MIN) AS M_QTY FROM MATERIAL ORDER BY M_QTY";
		Connection conn = DatabaseHelper.getConnect();
		try ( PreparedStatement stmt = conn.prepareStatement(sql)) {
			try (ResultSet rs = stmt.executeQuery()) {
				while (rs.next()) {
					ReportManager obj = ReportManager.fromRSMaterial(rs);
					list.add(obj);
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}
}
