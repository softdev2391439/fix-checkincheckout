/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.Receipt;
import softdecoffee.model.ReceiptDetail;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptDao implements Dao<Receipt> {

    private Receipt receipt;

    @Override
  /*public Receipt get(int id) {
        Receipt receipt = null;
        String sql = " SELECT * FROM RECEIPT WHERE RE_CODE =? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
                ReceiptDetailDao rdd = new ReceiptDetailDao();
                ArrayList<ReceiptDetail> receiptDetails = (ArrayList<ReceiptDetail>) rdd.getAll(" RE_CODE= " + receipt.getId(), " RCD_CODE ");
                receipt.setReceiptDetails(receiptDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
    }*/
    
    public Receipt get(int id) {
        Receipt receipt = null;
        String sql = " SELECT * FROM RECEIPT WHERE RE_CODE=? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
                ReceiptDetailDao rdd = new ReceiptDetailDao();
                ArrayList<ReceiptDetail> receiptDetails = (ArrayList<ReceiptDetail>) rdd.getAll(" RE_CODE= "+receipt.getId(), " RCD_CODE ");
                receipt.setReceiptDetails(receiptDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
    }
    
    

    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = " SELECT * FROM RECEIPT ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Receipt> getAll(String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {
        String sql = "INSERT INTO RECEIPT (EMP_CODE, CUS_CODE, RE_TOTAL, RE_PAYMENT, RE_CHANGE, RE_NET_TOTAL, "
                + "RE_DISCOUNT, RE_TOTAL_QTY) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, obj.getEmpId());
            stmt.setInt(2, obj.getCusId());
            stmt.setFloat(3, obj.getReceiptTotal());
            stmt.setFloat(4, obj.getReceiptPayment());
            stmt.setFloat(5, obj.getReceiptChange());
            stmt.setFloat(6, obj.getReceiptNetTotal());
            stmt.setFloat(7, obj.getReceiptDiscount());
            stmt.setInt(8, obj.getReceiptTotalQty());

            stmt.executeUpdate();
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                int id = generatedKeys.getInt(1);
                obj.setId(id);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

        return obj;
    }

    public Receipt update(Receipt obj) {
        String sql = "UPDATE RECEIPT SET EMP_CODE = ?, CUS_CODE = ?, RE_TOTAL = ?, RE_PAYMENT = ?, RE_CHANGE = ?, "
                + "RE_NET_TOTAL = ?, RE_DISCOUNT = ?, RE_TOTAL_QTY = ? WHERE RE_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());
            stmt.setInt(2, obj.getCusId());
            stmt.setFloat(3, obj.getReceiptTotal());
      
            stmt.setFloat(4, obj.getReceiptPayment());
            stmt.setFloat(5, obj.getReceiptChange());
            stmt.setFloat(6, obj.getReceiptNetTotal());
            stmt.setFloat(7, obj.getReceiptDiscount());
            stmt.setInt(8, obj.getReceiptTotalQty());
            //stmt.setInt(9, obj.getId());

            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Receipt obj) {
        String sql = "DELETE FROM RECEIPT WHERE RE_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return -1;
    }


}
