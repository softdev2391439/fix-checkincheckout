/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.StockBill;
import softdecoffee.model.Vendor;

/**
 *
 * @author ASUS
 */
public class StockBillDao implements Dao<StockBill> {

    @Override
    public StockBill get(int id) {
        StockBill stockBill = null;
        String sql = "SELECT * FROM BILL_STOCK WHERE BILL_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stockBill = StockBill.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stockBill;
    }

//    public StockBill getByVen(int id) {
//        StockBill stockBill = null;
//        String sql = "SELECT * FROM BILL_STOCK WHERE V_CODE=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, id);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                stockBill = StockBill.fromRS(rs);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return stockBill;
//    }
    public StockBill getLast() {
        StockBill stockBill = null;
        String sql = "SELECT * FROM BILL_STOCK ORDER BY BILL_CODE DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                stockBill = StockBill.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stockBill;
    }

    public List<StockBill> getAll() {
        ArrayList<StockBill> list = new ArrayList<>();
        String sql = "SELECT * FROM BILL_STOCK";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockBill stockBill = StockBill.fromRS(rs);
                list.add(stockBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<StockBill> getAll(String where, String order) {
        ArrayList<StockBill> list = new ArrayList<>();
        String sql = "SELECT * FROM BILL_STOCK where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockBill stockBill = StockBill.fromRS(rs);
                list.add(stockBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<StockBill> getAll(String order) {
        ArrayList<StockBill> list = new ArrayList<>();
        String sql = "SELECT * FROM BILL_STOCK  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockBill stockBill = StockBill.fromRS(rs);
                list.add(stockBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<StockBill> getByVendor(int vendorId) {
//        String vendorFormat = String.format("V_CODE = %d", vendorId);
        StockBill stockBill;
        ArrayList<StockBill> list = new ArrayList();
        String sql = "SELECT * FROM BILL_STOCK WHERE V_CODE = ? ORDER BY V_CODE";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, vendorId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stockBill = StockBill.fromRS(rs);
                list.add(stockBill);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return list;
    }

    @Override
    public StockBill save(StockBill obj) {
        ArrayList<StockBill> list = new ArrayList<>();
        String sql = "INSERT INTO BILL_STOCK (BILL_TOTAL_PRICE, BILL_TOTAL_QTY, EMP_CODE, BILL_PAY, V_CODE)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotalPrice());
            stmt.setInt(2, obj.getTotalQty());
            stmt.setInt(3, obj.getEmployeeId());
            stmt.setDouble(4, obj.getBillPay());
            stmt.setInt(5, obj.getVendorId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public StockBill update(StockBill obj) {
        ArrayList<StockBill> list = new ArrayList<>();
        String sql = "UPDATE BILL_STOCK"
                + " SET BILL_TOTAL_PRICE = ?, BILL_TOTAL_QTY = ?, EMP_CODE = ?, BILL_PAY = ?, V_CODE = ?"
                + " WHERE BILL_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotalPrice());
            stmt.setInt(2, obj.getTotalQty());
            stmt.setInt(3, obj.getEmployeeId());
            stmt.setDouble(4, obj.getBillPay());
            stmt.setInt(5, obj.getVendorId());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(StockBill obj) {
        String sql = "DELETE FROM BILL_STOCK WHERE BILL_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
