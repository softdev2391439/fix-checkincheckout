/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.StockBill;
import softdecoffee.model.StockBillDetail;

/**
 *
 * @author Nobpharat
 */
public class StockBillDetailDao implements Dao<StockBillDetail> {

    @Override
    public StockBillDetail get(int id) {
        StockBillDetail stockBillDetail = null;
        String sql = "SELECT * FROM BILL_DETAIL WHERE BD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stockBillDetail = StockBillDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stockBillDetail;
    }
    
    public StockBillDetail getLast() {
        StockBillDetail stockBillDetail = null;
        String sql = "SELECT * FROM BILL_DETAIL ORDER BY BD_CODE DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                stockBillDetail = StockBillDetail.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stockBillDetail;
    }
    
    public List<StockBillDetail> getByCheckId(int id) {
        ArrayList<StockBillDetail> list = new ArrayList();
        String sql = "SELECT * FROM BILL_DETAIL "
                + "WHERE BILL_CODE = " + id;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockBillDetail stockBillDetail = StockBillDetail.fromRS(rs);
                list.add(stockBillDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    
    public List<StockBillDetail> getAll() {
        ArrayList<StockBillDetail> list = new ArrayList();
        String sql = "SELECT * FROM BILL_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockBillDetail stockBillDetail = StockBillDetail.fromRS(rs);
                list.add(stockBillDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public ArrayList<StockBillDetail> getAllByStockBillId(int id) {
        ArrayList<StockBillDetail> list = new ArrayList();
        String sql = "SELECT * FROM BILL_DETAIL WHERE BILL_CODE = " + id + " ORDER BY BD_CODE ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockBillDetail stockBillDetail = StockBillDetail.fromRS(rs);
                list.add(stockBillDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<StockBillDetail> getAll(String order) {
        ArrayList<StockBillDetail> list = new ArrayList();
        String sql = "SELECT * FROM BILL_DETAIL ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockBillDetail stockBillDetail = StockBillDetail.fromRS(rs);
                list.add(stockBillDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockBillDetail save(StockBillDetail obj) {

        String sql = "INSERT INTO BILL_DETAIL (BILL_CODE, BD_AMOUNT , BD_PRICE_PER_UNIT, BD_PRICE_TOTAL, M_CODE)"
                + "VALUES( ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBiiId());
            stmt.setDouble(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
            stmt.setDouble(4, obj.getTotalPrrice());
            stmt.setInt(5, obj.getMaterrialId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public StockBillDetail update(StockBillDetail obj) {
        String sql = "UPDATE BILL_DETAIL SET "
                + "BILL_CODE = ?, "
                + "BD_AMOUNT = ?, "
                + "BD_PRICE_PER_UNIT = ?, "
                + "BD_PRICE_TOTAL = ?, "
                + "M_CODE = ? "
                + "WHERE BD_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBiiId());
            stmt.setDouble(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
            stmt.setDouble(4, obj.getTotalPrrice());
            stmt.setInt(5, obj.getMaterrialId());
            stmt.setInt(6, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(StockBillDetail obj) {
        String sql = "DELETE FROM BILL_DETAIL WHERE BD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
    public int deleteAllFromStockBillID(StockBill stockBill) {       
        String sql = "DELETE FROM BILL_DETAIL WHERE BILL_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, stockBill.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    public List<StockBillDetail> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
