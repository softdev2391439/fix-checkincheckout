/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.Vendor;

/**
 *
 * @author Puri
 */
public class VendorDao implements Dao<Vendor> {

    @Override
    public Vendor get(int id) {
        Vendor vendor = null;
        String sql = "SELECT * FROM VENDOR WHERE V_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                vendor = Vendor.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return vendor;
    }
    
    public Vendor getByTel(String tel) {
        Vendor vendor = null;
        String sql = "SELECT * FROM vendor WHERE v_phone=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                vendor = Vendor.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return vendor;
    }

    public List<Vendor> getAll() {
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM VENDOR";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Vendor vendor = Vendor.fromRS(rs);
                list.add(vendor);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Vendor> getAll(String where, String order) {
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM VENDOR where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Vendor vendor = Vendor.fromRS(rs);
                list.add(vendor);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Vendor> getAll(String order) {
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM VENDOR  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Vendor vendor = Vendor.fromRS(rs);
                list.add(vendor);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Vendor save(Vendor obj) {

        String sql = "INSERT INTO VENDOR (V_COUNTRY, V_ADDRESS, V_PHONE, V_NAME, V_CONTACT, V_CATEGORY)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCountry());
            stmt.setString(2, obj.getAddress());
            stmt.setString(3, obj.getTel());
            stmt.setString(4, obj.getName());
            stmt.setString(5, obj.getContact());
            stmt.setString(6, obj.getCategory());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Vendor update(Vendor obj) {
        String sql = "UPDATE VENDOR"
                + " SET V_COUNTRY = ?, V_ADDRESS = ?, V_PHONE = ?, V_NAME = ?, V_CONTACT = ?, V_CATEGORY = ?"
                + " WHERE V_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCountry());
            stmt.setString(2, obj.getAddress());
            stmt.setString(3, obj.getTel());
            stmt.setString(4, obj.getName());
            stmt.setString(5, obj.getContact());
            stmt.setString(6, obj.getCategory());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Vendor obj) {
        String sql = "DELETE FROM VENDOR WHERE V_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
 
}
