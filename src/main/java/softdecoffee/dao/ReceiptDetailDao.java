/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.ReceiptDetail;

/**
 *
 * @author Admin
 */
public class ReceiptDetailDao implements Dao<ReceiptDetail> {

    public static String getProductNameByProductCode;

    @Override
    public ReceiptDetail get(int id) {
        ReceiptDetail receiptDetail = null;
        String sql = "SELECT * FROM RECEIPT_DETAIL WHERE RCD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receiptDetail = ReceiptDetail.fromRs(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receiptDetail;
    }

    public static String getProductNameByProductCode(int productCode) {
        String productName = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT PROD_NAME FROM PRODUCT WHERE PROD_CODE = ?";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, productCode);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                productName = rs.getString("PROD_NAME");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return productName;
    }

    public List<ReceiptDetail> getAll() {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRs(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ReceiptDetail> getAll(String where, String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_DETAIL where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRs(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReceiptDetail> getAll(String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_DETAIL ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRs(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptDetail save(ReceiptDetail obj) {
        String sql = "INSERT INTO RECEIPT_DETAIL (PROD_CODE, RCD_UNIT_PRICE, RCD_QTY, RCD_TOTAL, RC_CODE) VALUES (?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();

        try (PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setInt(1, obj.getProductId());
            stmt.setFloat(2, obj.getReceiptDetailUnitPrice());
            stmt.setInt(3, obj.getReceiptDetailQty());
            stmt.setFloat(4, obj.getReceiptDetailTotal());
            stmt.setInt(5, obj.getReceiptId());

            int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 1) {
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int id = generatedKeys.getInt(1);
                    obj.setId(id);
                    return obj;
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

        return obj;
    }

    @Override
    public ReceiptDetail update(ReceiptDetail obj) {
        String sql = "UPDATE RECEIPT_DETAIL SET PROD_CODE = ?, RCD_UNIT_PRICE = ?, RCD_QTY = ?, RCD_TOTAL = ?, RC_CODE = ? WHERE RCD_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProductId());
            stmt.setFloat(2, obj.getReceiptDetailUnitPrice());
            stmt.setInt(3, obj.getReceiptDetailQty());
            stmt.setFloat(4, obj.getReceiptDetailTotal());
            stmt.setInt(5, obj.getReceiptId());
            stmt.setInt(6, obj.getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptDetail obj) {
        String sql = "DELETE FROM RECEIPT_DETAIL WHERE RCD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return -1;
    }
}
