/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package softdecoffee.ui;

/**
 *
 * @author Lenovo
 */
import softdecoffee.ui.main.MainMemuOfManager;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import softdecoffee.component.MaterialPanel;
import softdecoffee.dao.CheckStockDao;
import softdecoffee.dao.CheckStockDetailDao;
import softdecoffee.dao.EmployeeDao;
import softdecoffee.model.CheckStock;
import softdecoffee.model.CheckStockDetail;
import softdecoffee.model.Employee;
import softdecoffee.service.CheckStockDetailService;
import softdecoffee.service.CheckStockService;
import softdecoffee.ui.main.MainMemuOfStaff;

/**
 *
 * @author Lenovo
 */
public class CheckStockPanel extends javax.swing.JPanel {

	private ArrayList<CheckStock> checkStocks;
	private CheckStockService checkStockService = new CheckStockService();
	private CheckStockDetailService checkStockDetailService;
	private CheckStockDao checkStockDao = new CheckStockDao();
	private CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
	private CheckStockDetail checkStockDetail;
	private CheckStock checkStock;
	private CheckStock editedCheckStock;
	private CheckStock deletedCheckStock;
	private String currentUser;
	private Employee EmpUser = new Employee();
	private String rank;

	/**
	 * Creates new form CheckStockPanel
	 */
	// ต้อง เพิ่ม current User เพื่อใช้ในการ assign ค่า ใน parameter ของ checkStock
	public CheckStockPanel(String username, String rank) {
		initComponents();
		this.currentUser = username;
		this.rank = rank;
		initCheckStockTable();
		checkStock = new CheckStock();
		if (checkStockDetailDao.getLastByLastestCheckStockID().getId() != checkStock.getId()) {
			if (checkStockDetailDao.getLastByLastestCheckStockID().getId() > checkStock.getId()) {
				checkStock.setId(checkStockDetailDao.getLastByLastestCheckStockID().getId());
			} else {
				checkStockDetail.setCheckStockId(checkStock.getId());
			}
		}

		EmpUser = EmployeeDao.getByUserName(currentUser);
		lblUser.setText("User: " + EmpUser.getFname() + " " + EmpUser.getLname());

		tblCheckStockDetail.setModel(new AbstractTableModel() {
			String[] headers = {"id", "Material id", "CheckStock id", "Remaining Amount", "Expired Amount", "Total lost"};

			@Override
			public String getColumnName(int column) {
				return headers[column];
			}

			@Override
			public int getRowCount() {
				//ไว้ดูข้อมูล
				System.out.println("checkStock = " + checkStock);
				System.out.println("checkStock = " + checkStock.getCheckStockDetails());
				System.out.println("getRowCount = " + checkStock.getCheckStockDetails().size());
				//
				return checkStock.getCheckStockDetails().size();
			}

			@Override
			public int getColumnCount() {
				return 6;
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				ArrayList<CheckStockDetail> checkStockDetails = checkStock.getCheckStockDetails();
				checkStockDetail = checkStockDetails.get(rowIndex);
				switch (columnIndex) {
					case 0:
						return checkStockDetail.getId();
					case 1:
						return checkStockDetail.getMaterialId();
					case 2:
						return checkStockDetail.getCheckStockId();
					case 3:
						return checkStockDetail.getRemainingAmount();
					case 4:
						return checkStockDetail.getExpAmount();
					case 5:
						return checkStockDetail.getTotalLost();
					default:
						return "";
				}
			}
		});
	}

	private void initCheckStockTable() {
		checkStocks = checkStockService.getCheckStocksOrderById();
		tblCheckStock.getTableHeader().setFont(new Font("TH Saraban New", Font.PLAIN, 16));
		tblCheckStock.setRowHeight(100);
		tblCheckStock.setModel(new AbstractTableModel() {
			String[] headers = {"ID", "Employee ID", "DATETIME"};

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex) {
					default:
						return String.class;
				}
			}

			@Override
			public String getColumnName(int column) {
				return headers[column];
			}

			@Override
			public int getRowCount() {
				return checkStocks.size();
			}

			@Override
			public int getColumnCount() {
				return 3;
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CheckStock checkStockTable = checkStocks.get(rowIndex);
				switch (columnIndex) {
					case 0:
						return checkStockTable.getId();
					case 1:
						return checkStockTable.getEmployeeId();
					case 2:
						return checkStockTable.getCheckDate();
					default:
						return "";
				}

			}

		});

		tblCheckStock.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = tblCheckStock.rowAtPoint(e.getPoint());
				CheckStock editcheckStock = checkStocks.get(row);
				int id = editcheckStock.getId();

				ArrayList<CheckStockDetail> checkStockDetails = (ArrayList<CheckStockDetail>) checkStockDetailDao.getByCheckId(id);
				checkStock.setCheckStockDetails(checkStockDetails);

				//ไว้ดูเฉยๆ
				System.out.println(id);
				System.out.println(checkStock.getCheckStockDetails());
				//
				refreshCheckStockDetail();
			}
		});
	}

	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        pnlHeader = new javax.swing.JPanel();
        lblUser = new javax.swing.JLabel();
        pnlCheckStock = new javax.swing.JPanel();
        scrCheckStock = new javax.swing.JScrollPane();
        tblCheckStock = new javax.swing.JTable();
        pnlCheckStockDetail = new javax.swing.JPanel();
        scrCheckStockDetail = new javax.swing.JScrollPane();
        tblCheckStockDetail = new javax.swing.JTable();
        pnlButton = new javax.swing.JPanel();
        btnMaterial = new javax.swing.JButton();
        btnAddCheckStock = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        jPanel2.setBackground(new java.awt.Color(221, 206, 183));

        pnlHeader.setBackground(new java.awt.Color(171, 146, 131));

        lblUser.setFont(new java.awt.Font("Poppins", 0, 18)); // NOI18N
        lblUser.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUser.setText("User :");

        javax.swing.GroupLayout pnlHeaderLayout = new javax.swing.GroupLayout(pnlHeader);
        pnlHeader.setLayout(pnlHeaderLayout);
        pnlHeaderLayout.setHorizontalGroup(
            pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlHeaderLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblUser, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlHeaderLayout.setVerticalGroup(
            pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblUser)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pnlCheckStock.setBackground(new java.awt.Color(240, 228, 213));

        tblCheckStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrCheckStock.setViewportView(tblCheckStock);

        javax.swing.GroupLayout pnlCheckStockLayout = new javax.swing.GroupLayout(pnlCheckStock);
        pnlCheckStock.setLayout(pnlCheckStockLayout);
        pnlCheckStockLayout.setHorizontalGroup(
            pnlCheckStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCheckStockLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlCheckStockLayout.setVerticalGroup(
            pnlCheckStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCheckStockLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStock, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlCheckStockDetail.setBackground(new java.awt.Color(240, 228, 213));

        tblCheckStockDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrCheckStockDetail.setViewportView(tblCheckStockDetail);

        javax.swing.GroupLayout pnlCheckStockDetailLayout = new javax.swing.GroupLayout(pnlCheckStockDetail);
        pnlCheckStockDetail.setLayout(pnlCheckStockDetailLayout);
        pnlCheckStockDetailLayout.setHorizontalGroup(
            pnlCheckStockDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCheckStockDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStockDetail, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlCheckStockDetailLayout.setVerticalGroup(
            pnlCheckStockDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCheckStockDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStockDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlButton.setBackground(new java.awt.Color(171, 146, 131));

        btnMaterial.setBackground(new java.awt.Color(242, 199, 99));
        btnMaterial.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        btnMaterial.setText("Material");
        btnMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaterialActionPerformed(evt);
            }
        });

        btnAddCheckStock.setBackground(new java.awt.Color(242, 199, 99));
        btnAddCheckStock.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        btnAddCheckStock.setText("ADD");
        btnAddCheckStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddCheckStockActionPerformed(evt);
            }
        });

        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
        pnlButton.setLayout(pnlButtonLayout);
        pnlButtonLayout.setHorizontalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnMaterial)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAddCheckStock)
                .addContainerGap())
        );
        pnlButtonLayout.setVerticalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                        .addComponent(btnAddCheckStock, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(pnlCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlCheckStockDetail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlCheckStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlCheckStockDetail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 758, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 412, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 6, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 7, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMaterialActionPerformed
	    if (rank == "Manager") {
		    MainMemuOfManager mainMemuOfManager;
		    mainMemuOfManager = (MainMemuOfManager) SwingUtilities.getWindowAncestor(this);
		    mainMemuOfManager.setScrPanel(new MaterialPanel(currentUser, rank), "Stock Management");
		    mainMemuOfManager.revalidate();
	    } else if (rank == "Staff") {
		    MainMemuOfStaff mainMemuOfStaff;
		    mainMemuOfStaff = (MainMemuOfStaff) SwingUtilities.getWindowAncestor(this);
		    mainMemuOfStaff.setScrPanel(new MaterialPanel(currentUser, rank), "Stock Management");
		    mainMemuOfStaff.revalidate();
	    }


    }//GEN-LAST:event_btnMaterialActionPerformed

    private void btnAddCheckStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddCheckStockActionPerformed
	    editedCheckStock = new CheckStock();
	    openDialog();
    }//GEN-LAST:event_btnAddCheckStockActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
	    int selectedIndex = tblCheckStock.getSelectedRow();
	    if (selectedIndex >= 0) {
		    System.out.println("selectindex = " + selectedIndex);
		    deletedCheckStock = checkStocks.get(selectedIndex);
		    System.out.println("selectcheckStock = " + deletedCheckStock);
		    int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
			    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
		    if (input == 0) {
			    checkStockDetailDao.deleteAllFromCheckStockID(deletedCheckStock);
			    checkStockDao.delete(deletedCheckStock);
			    refreshTableWhenDelete();
		    }
	    }
    }//GEN-LAST:event_btnDeleteActionPerformed

	private void openDialog() {
		JFrame frame = (JFrame) SwingUtilities.getRoot(this);
		CheckStockDialog userDialog = new CheckStockDialog(frame, editedCheckStock, currentUser,rank);
		userDialog.setLocationRelativeTo(this);
		userDialog.setVisible(true);
		userDialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				refreshTable();
			}

		});
	}

	private void openPanel() {
		MainMemuOfManager mainMemuOfManager;
		mainMemuOfManager = (MainMemuOfManager) SwingUtilities.getWindowAncestor(this);
		mainMemuOfManager.setScrPanel(new MaterialPanel(currentUser, rank), "Material Panel");
		mainMemuOfManager.revalidate();

	}

	private void refreshTable() {
		checkStocks = checkStockService.getCheckStocksArray();
		tblCheckStock.revalidate();
		tblCheckStock.repaint();
		tblCheckStockDetail.revalidate();
		tblCheckStockDetail.repaint();
	}

	private void refreshTableWhenDelete() {
		checkStocks = checkStockService.getCheckStocksArray();
		checkStock = new CheckStock();
		tblCheckStock.revalidate();
		tblCheckStock.repaint();
		tblCheckStockDetail.revalidate();
		tblCheckStockDetail.repaint();
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddCheckStock;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnMaterial;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblUser;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlCheckStock;
    private javax.swing.JPanel pnlCheckStockDetail;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.JScrollPane scrCheckStock;
    private javax.swing.JScrollPane scrCheckStockDetail;
    private javax.swing.JTable tblCheckStock;
    private javax.swing.JTable tblCheckStockDetail;
    // End of variables declaration//GEN-END:variables
    public void refreshCheckStock() {
		tblCheckStock.revalidate();
		tblCheckStock.repaint();
	}

	private void refreshCheckStockDetail() {
		tblCheckStockDetail.revalidate();
	}
}
