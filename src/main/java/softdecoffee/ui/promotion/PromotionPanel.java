/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package softdecoffee.ui.promotion;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import softdecoffee.dao.PromotionDao;
import softdecoffee.dao.PromotionDetailDao;
import softdecoffee.model.Customer;
import softdecoffee.model.Promotion;
import softdecoffee.model.PromotionDetail;
import softdecoffee.model.Promotion;
import softdecoffee.model.PromotionDetail;
import softdecoffee.service.PromotionService;
import softdecoffee.service.PromotionDetailService;

/**
 *
 * @author basba
 */
public class PromotionPanel extends javax.swing.JDialog {

    private PromotionService PromotionService = new PromotionService();;
    private PromotionDetailService promotionDetailService = new PromotionDetailService();
    private List<Promotion> promo;
    private Promotion editedPromotion;
    private List<PromotionDetail> prode;
    private ArrayList<PromotionDetail> promotionDetails;
    Promotion promotion;
    private Promotion showPromotion;

    /**
     * Creates new form PromotionPanel
     */
    public PromotionPanel(JFrame parent, Promotion showPromotion) {
        super(parent, true);
        initComponents();
        this.showPromotion = showPromotion;
        promotion = new Promotion();
        
        initPromotionTable();   
        tblProdetail.setRowHeight(25);
        tblProdetail.setModel(new AbstractTableModel() {
            String[] columNames = {"NO.", "ID", "Promotion Code", "Name", "Detail", "Discount"};

            @Override
            public String getColumnName(int column) {
                return columNames[column];
            }

            public int getRowCount() {
                return promotion.getPromotionDetails().size();
            }

            public int getColumnCount() {
                return 6;
            }

            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<PromotionDetail> promotionDetails = promotion.getPromotionDetails();               
                PromotionDetail promotionDetail = promotionDetails.get(rowIndex);
                Promotion promotionlist = promotion;
                switch (columnIndex) {
                    case 0:
                        return (rowIndex + 1);
                    case 1:
                        return promotionDetail.getProId();
                    case 2:
                        return promotionlist.getId();
                    case 3:
                        return promotionlist.getName();
                    case 4:
                        return promotionlist.getDetail();
                    case 5:
                        return promotionDetail.getDiscount();

                    default:
                        return "Unknow";
                }

            }
        });
        
        
        refreshProdetail();

    }

    private void initPromotionTable() {
        promo = PromotionService.getPromotions();
        tblProlist.setRowHeight(25);
        tblProlist.setModel(new AbstractTableModel() {
            String[] columNames = {"NO.", "Name", "StartDatel", "EndDatel", "Status"};

            @Override
            public String getColumnName(int column) {
                return columNames[column];
            }

            public int getRowCount() {
                return promo.size();
            }

            public int getColumnCount() {
                return 5;
            }

            public Object getValueAt(int rowIndex, int columnIndex) {
                Promotion promotionTable = promo.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return rowIndex + 1;
                    case 1:
                        return promotionTable.getName();
                    case 2:
                        return promotionTable.getStartDate();
                    case 3:
                        return promotionTable.getEndDate();
                    case 4:
                        return promotionTable.getStatus();

                    default:
                        return "Unknow";
                }
            }
        });
        tblProlist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblProlist.rowAtPoint(e.getPoint());
                Promotion editpromotion = promo.get(row);
                int id = editpromotion.getId();
                System.out.println(id);
                System.out.println("selected promotion : "+editpromotion);

                PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
                promotionDetails = (ArrayList<PromotionDetail>) promotionDetailDao.getById(id);
                System.out.println("mouseclick" + promotionDetailDao.getById(id));
                
                PromotionDao promotionDao = new PromotionDao();
                promotion = promotionDao.get(id);
                promotion.setPromotionDetails(promotionDetails);
                System.out.println("promotionDetail = " + promotion.getPromotionDetails());

                refreshProdetail();
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton3 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProlist = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProdetail = new javax.swing.JTable();
        prolist = new javax.swing.JLabel();
        prodetail = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnDelete = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();

        jButton3.setText("jButton3");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        setBackground(new java.awt.Color(171, 146, 131));

        jPanel1.setBackground(new java.awt.Color(221, 206, 183));

        tblProlist.setFont(new java.awt.Font("Poppins", 0, 12)); // NOI18N
        tblProlist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblProlist);

        tblProdetail.setFont(new java.awt.Font("Poppins", 0, 12)); // NOI18N
        tblProdetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblProdetail);

        prolist.setFont(new java.awt.Font("Poppins", 1, 18)); // NOI18N
        prolist.setForeground(new java.awt.Color(51, 51, 51));
        prolist.setText("Promotion list");

        prodetail.setFont(new java.awt.Font("Poppins", 1, 18)); // NOI18N
        prodetail.setForeground(new java.awt.Color(51, 51, 51));
        prodetail.setText("Promotion detail");

        jPanel2.setBackground(new java.awt.Color(171, 146, 131));

        btnDelete.setBackground(new java.awt.Color(242, 199, 99));
        btnDelete.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        btnDelete.setText("DELETE");
        btnDelete.setPreferredSize(new java.awt.Dimension(72, 29));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(242, 199, 99));
        btnEdit.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        btnEdit.setText("EDIT");
        btnEdit.setPreferredSize(new java.awt.Dimension(72, 29));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnAdd.setBackground(new java.awt.Color(242, 199, 99));
        btnAdd.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        btnAdd.setText("ADD");
        btnAdd.setPreferredSize(new java.awt.Dimension(72, 29));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 908, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(prolist)
                            .addComponent(prodetail))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(prolist)
                .addGap(5, 5, 5)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prodetail)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedPromotion = new Promotion();
        openDialog();
    }//GEN-LAST:event_btnAddActionPerformed

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PromotionDialog promotionDialog = new PromotionDialog(frame, editedPromotion);
        promotionDialog.setVisible(true);
        promotionDialog.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

        });
        refreshTable();
    }

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblProlist.getSelectedRow();
        if (selectedIndex >= 0) {
            editedPromotion = promo.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                PromotionService.delete(editedPromotion);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = tblProlist.getSelectedRow();
        if (selectedIndex >= 0) {
            editedPromotion = promo.get(selectedIndex);
            openDialog();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    public void refreshTable() {
        promo = PromotionService.getPromotions();
        tblProlist.revalidate();
        tblProlist.repaint();
    }

    private void refreshProdetail() {
        tblProdetail.revalidate();
        tblProdetail.repaint();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel prodetail;
    private javax.swing.JLabel prolist;
    private javax.swing.JTable tblProdetail;
    private javax.swing.JTable tblProlist;
    // End of variables declaration//GEN-END:variables
}
