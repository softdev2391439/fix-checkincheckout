/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package softdecoffee.ui;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import softdecoffee.component.EditeEmpDialog;
import softdecoffee.model.Employee;
import softdecoffee.service.EmployeeService;

public class EmPanel extends javax.swing.JPanel {

	private List<Employee> list;
	private final EmployeeService employeeService;
	private Object editedEmployee;

	public EmPanel() {
		initComponents();
		employeeService = new EmployeeService();
		list = employeeService.getEmployees();
		tblEm.setRowHeight(25);
		tblEm.setModel(new AbstractTableModel() {

			String[] columnNames = {"ID", "First Name", "Last Name", "Role", "Tel", "Email", "Address", "Indate", "Status", "", ""};

			@Override
			public String getColumnName(int column) {
				return columnNames[column];
			}

			@Override
			public int getRowCount() {
				return list.size();
			}

			@Override
			public int getColumnCount() {
				return 11;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex) {
					case 9:
					case 10:
						return ImageIcon.class;
					default:
						return String.class;
				}
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				Employee employee = list.get(rowIndex);
				switch (columnIndex) {
					case 0:
						return employee.getId();
					case 1:
						return employee.getFname();
					case 2:
						return employee.getLname();
					case 3:
						return employee.getPosition();
					case 4:
						return employee.getTel();
					case 5:
						return employee.getEmail();
					case 6:
						return employee.getAddress();
					case 7:
						return employee.getIndate();
					case 8:
						return employee.getStatus();
					case 9:
						ImageIcon iconEdit = setImage("./image/icon/Edite.png");
						return iconEdit;
					case 10:
						ImageIcon iconDelete = setImage("./image/icon/delete.png");
						return iconDelete;
					default:
						return "Unknown";
				}
			}

			private ImageIcon setImage(String path) {
				ImageIcon icon = new ImageIcon(path);
				Image image = icon.getImage();
				int width = image.getWidth(null);
				int height = image.getHeight(null);
				Image newImage = image.getScaledInstance((int) (20 * ((float) width / height)), 20, Image.SCALE_SMOOTH);
				return new ImageIcon(newImage);
			}
		});
		tblEm.getColumnModel().getColumn(9).setPreferredWidth(25);
		 tblEm.getColumnModel().getColumn(10).setPreferredWidth(25);
		 tblEm.getColumnModel().getColumn(9).setResizable(false);
		 tblEm.getColumnModel().getColumn(10).setResizable(false);

		tblEm.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int row = tblEm.rowAtPoint(e.getPoint());
				int col = tblEm.columnAtPoint(e.getPoint());
				if (col == 9) {
					int selectedIndex = tblEm.getSelectedRow();
					if (selectedIndex >= 0) {
						editedEmployee = list.get(selectedIndex);
						openDialog();
					}
				} else if (col == 10) {
					int selectedIndex = tblEm.getSelectedRow();
					if (selectedIndex >= 0) {
						editedEmployee = list.get(selectedIndex);
						int input = JOptionPane.showConfirmDialog(tblEm, "Do you want to proceed?", "Select an Option...",
							JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
						if (input == 0) {
							employeeService.delete((Employee) editedEmployee);
						}
						refreshTable();
					}
				}
			}
		});
	}

	/**
	 * This method is called from within the constructor to initialize the
	 * form. WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEm = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(221, 206, 183));

        tblEm.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblEm);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 701, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(171, 146, 131));

        jButton3.setBackground(new java.awt.Color(242, 199, 99));
        jButton3.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jButton3.setText("Back");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        btnAdd.setBackground(new java.awt.Color(242, 199, 99));
        btnAdd.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        jButton5.setBackground(new java.awt.Color(242, 199, 99));
        jButton5.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jButton5.setText("Pay");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAdd)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton5)
                    .addComponent(btnAdd))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
	    editedEmployee = new Employee();
	    openDialog();
    }//GEN-LAST:event_btnAddActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

	private void refreshTable() {
		list = employeeService.getEmployees();
		tblEm.revalidate();
		tblEm.repaint();
	}


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblEm;
    // End of variables declaration//GEN-END:variables

	private void openDialog() {
		JFrame frame = (JFrame) SwingUtilities.getRoot(this);
		EditeEmpDialog editeEmpDialog = new EditeEmpDialog(frame, (Employee) editedEmployee);
		editeEmpDialog.setLocationRelativeTo(this);
		editeEmpDialog.setVisible(true);
		editeEmpDialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				super.windowClosed(e); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
				refreshTable();
			}
		});
	}

}
