/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package softdecoffee.component;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.Timer;
import javax.swing.filechooser.FileNameExtensionFilter;
import softdecoffee.model.UtilityCost;
import softdecoffee.service.UtilityCostService;

/**
 *
 * @author Nobpharat
 */
public class UtilityCostDialog extends javax.swing.JDialog {

	//private final UtilityCostService utilityCostServic;
	private UtilityCost editedUtilityCost;
	private String pathElec;
	private String pathWater;
	private String pathRent;

	/**
	 * Creates new form UtilityCostDialog
	 */
//	public UtilityCostDialog(java.awt.Frame parent, UtilityCost editUtilityCost) {
//		super(parent, true);
//		initComponents();
//		this.editedUtilityCost = editUtilityCost;
//		setObjectToForm();
//		utilityCostServic = new UtilityCostService();
//		loadImage();
//		startDateTimeUpdater();
//
//	}

//	private void startDateTimeUpdater() {
//		Timer timer = new Timer(1000, e -> {
//			LocalDateTime localDateTime = LocalDateTime.now();
//			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//			String formattedDateTime = localDateTime.format(formatter);
//			lblDate.setText(formattedDateTime);
//		});
//		timer.setRepeats(true);
//		timer.start();
//	}

	private void loadImage() {
		if (editedUtilityCost.getId() > 0) {
			int id = editedUtilityCost.getId();
			loadImageAndSetIcon("./image/utility cost/UCE" + id + ".png", lblPhotoElectric);
			loadImageAndSetIcon("./image/utility cost/UCW" + id + ".png", lblPhotoWater);
			loadImageAndSetIcon("./image/utility cost/UCR" + id + ".png", lblPhotoRental);
		}
	}

	private void loadImageAndSetIcon(String imagePath, JLabel label) {
		ImageIcon icon = new ImageIcon(imagePath);
		Image image = icon.getImage().getScaledInstance(280, 450, Image.SCALE_SMOOTH);
		label.setIcon(new ImageIcon(image));
	}

	public void chooseImage(JLabel label) {
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Image file", "png", "jpg");
		fileChooser.setFileFilter(filter);
		int returnVal = fileChooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			System.out.println("Selected file: " + file.getAbsolutePath());
			loadImage(file.getAbsolutePath(), label);
			if (label == lblPhotoElectric) {
				pathElec = file.getAbsolutePath();
			} else if (label == lblPhotoWater) {
				pathWater = file.getAbsolutePath();
			} else if (label == lblPhotoRental) {
				pathRent = file.getAbsolutePath();
			}
		}
	}

	private void loadImage(String path, JLabel label) {
		if (editedUtilityCost.getId() > 0) {
			ImageIcon icon = new ImageIcon(path);
			Image image = icon.getImage();
			Image newImage = image.getScaledInstance(280, 450, Image.SCALE_SMOOTH);
			ImageIcon newIcon = new ImageIcon(newImage);
			label.setIcon(newIcon);
		}
	}

	private void saveImage(UtilityCost utilityCost) {
		if (pathElec == null || pathElec.isEmpty()) {
			return;
		} else if (pathWater == null || pathWater.isEmpty()) {
			return;
		} else if (pathRent == null || pathRent.isEmpty()) {
			return;
		}
		try {
			BufferedImage imageUCE = ImageIO.read(new File(pathElec));
			BufferedImage imageUCW = ImageIO.read(new File(pathWater));
			BufferedImage imageUCR = ImageIO.read(new File(pathRent));
			ImageIO.write(imageUCE, "png", new File("./image/utility cost/UCE" + utilityCost.getId() + ".png"));
			ImageIO.write(imageUCW, "png", new File("./image/utility cost/UCW" + utilityCost.getId() + ".png"));
			ImageIO.write(imageUCR, "png", new File("./image/utility cost/UCR" + utilityCost.getId() + ".png"));
		} catch (IOException ex) {
			Logger.getLogger(EditeEmpDialog.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the
	 * form. WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblID = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        edtElectric = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        edtWater = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        edtRental = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        edtTotal = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        lblDateSave = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lblPhotoElectric = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        lblPhotoWater = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        lblPhotoRental = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 204));

        jPanel2.setBackground(new java.awt.Color(255, 204, 204));

        lblID.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        lblID.setText("ID:");

        jLabel2.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        jLabel2.setText("Electric Cost  :");

        edtElectric.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtElectricActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        jLabel3.setText("Baht");

        edtWater.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtWaterActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        jLabel6.setText("Water   Cost  :");

        jLabel7.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        jLabel7.setText("Baht");

        jLabel8.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        jLabel8.setText("Retal    Cost  :");

        edtRental.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtRentalActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        jLabel9.setText("Baht");

        jLabel10.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        jLabel10.setText("Total Utility Cost  :");

        edtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtTotalActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        jLabel11.setText("Baht");

        lblDateSave.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        lblDateSave.setText("Date :");

        lblDate.setFont(new java.awt.Font("Poppins", 0, 14)); // NOI18N
        lblDate.setText("Now :");

        btnSave.setBackground(new java.awt.Color(242, 199, 99));
        btnSave.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(242, 199, 99));
        btnClear.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblID, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel8))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(7, 7, 7)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(edtElectric, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(edtWater, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(edtRental, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel9)))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lblDateSave, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel10)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(edtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel11))))
                        .addGap(0, 33, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear)
                .addGap(13, 13, 13))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(lblID)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtElectric, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(edtWater, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(edtRental, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(edtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDateSave)
                .addGap(105, 105, 105)
                .addComponent(lblDate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnClear))
                .addGap(19, 19, 19))
        );

        lblPhotoElectric.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPhotoElectric.setText("Electric Bill");
        lblPhotoElectric.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        lblPhotoElectric.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPhotoElectric.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lblPhotoElectric.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPhotoElectricMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPhotoElectric, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPhotoElectric, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblPhotoWater.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPhotoWater.setText("Water Bill");
        lblPhotoWater.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        lblPhotoWater.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPhotoWater.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lblPhotoWater.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPhotoWaterMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 302, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblPhotoWater, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 471, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblPhotoWater, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)))
        );

        lblPhotoRental.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPhotoRental.setText("Rental Bill");
        lblPhotoRental.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        lblPhotoRental.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblPhotoRental.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lblPhotoRental.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPhotoRentalMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 290, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblPhotoRental, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 471, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblPhotoRental, javax.swing.GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 35, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 90, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

        private void edtElectricActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtElectricActionPerformed
		// TODO add your handling code here:
        }//GEN-LAST:event_edtElectricActionPerformed

        private void edtWaterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtWaterActionPerformed
		// TODO add your handling code here:
        }//GEN-LAST:event_edtWaterActionPerformed

        private void edtRentalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtRentalActionPerformed
		// TODO add your handling code here:
        }//GEN-LAST:event_edtRentalActionPerformed

        private void edtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtTotalActionPerformed
		// TODO add your handling code here:
        }//GEN-LAST:event_edtTotalActionPerformed

        private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		if (editedUtilityCost == null) {
			editedUtilityCost = new UtilityCost();

		}

		if (editedUtilityCost.getId() < 0) {//Add New
			setFormToObject();
			//enableForm(false);
			//utilityCostServic.addNew(editedUtilityCost);
		} else {
			setFormToObject();
			enableForm(false);
			//utilityCostServic.update(editedUtilityCost);
		}
		saveImage(editedUtilityCost);
		this.dispose();
        }//GEN-LAST:event_btnSaveActionPerformed

        private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
		enableForm(false);
		editedUtilityCost = null;
        }//GEN-LAST:event_btnClearActionPerformed

        private void lblPhotoElectricMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPhotoElectricMouseClicked
		chooseImage(lblPhotoElectric);
        }//GEN-LAST:event_lblPhotoElectricMouseClicked

        private void lblPhotoWaterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPhotoWaterMouseClicked
		chooseImage(lblPhotoWater);
        }//GEN-LAST:event_lblPhotoWaterMouseClicked

        private void lblPhotoRentalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPhotoRentalMouseClicked
		chooseImage(lblPhotoRental);
        }//GEN-LAST:event_lblPhotoRentalMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextField edtElectric;
    private javax.swing.JTextField edtRental;
    private javax.swing.JTextField edtTotal;
    private javax.swing.JTextField edtWater;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblDateSave;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblPhotoElectric;
    private javax.swing.JLabel lblPhotoRental;
    private javax.swing.JLabel lblPhotoWater;
    // End of variables declaration//GEN-END:variables
private void setFormToObject() {

		editedUtilityCost.setElectric(Double.parseDouble(edtElectric.getText()));
		editedUtilityCost.setWater(Double.parseDouble(edtWater.getText()));
		editedUtilityCost.setRental(Double.parseDouble(edtRental.getText()));
		editedUtilityCost.setTotal(Double.parseDouble(edtTotal.getText()));
		editedUtilityCost.setDateString(lblDate.getText());
	}

	private void setObjectToForm() {
		lblID.setText("ID : " + editedUtilityCost.getId());
		edtElectric.setText(String.valueOf(editedUtilityCost.getElectric()));
		edtWater.setText(String.valueOf(editedUtilityCost.getWater()));
		edtRental.setText(String.valueOf(editedUtilityCost.getRental()));
		edtTotal.setText(String.valueOf(editedUtilityCost.getTotal()));
		lblDateSave.setText("Last change: " + editedUtilityCost.getDate());
	}

	private void enableForm(boolean b) {
		if (!b) {
			clearFormFields();
		}

	}

	private void clearFormFields() {
		lblID.setText("ID : -1");
		edtElectric.setText("0");
		edtWater.setText("0");
		edtRental.setText("0");
		edtTotal.setText("0");
		lblDateSave.setText("------------------------");
	}

}
