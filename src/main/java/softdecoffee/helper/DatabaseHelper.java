/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nobpharat
 */
public class DatabaseHelper {
    private static Connection conn = null;
    private static final String URL = "jdbc:sqlite:scdecoffee.db";
    static {
        getConnect();
    }
    public static synchronized Connection getConnect() {
        if(conn==null) {
            try {
                conn = DriverManager.getConnection(URL);
                System.out.println("Connection to SQLite has been establish.");
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return conn;
    }
    
    public static synchronized void close() {
        if(conn!=null) {
            try {
                conn.close();
                conn = null;
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static int getInsertedId(Statement stmt) {
        //สำหรับคนที่เคยใช้ตัวบนแล้วใช้ได้ก้ใช้ไป for people who successfully used this code group  
        
//        try {
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            return key.getInt(1);
//        } catch (SQLException ex) {
//            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return -1;


        //สำหรับคนที่ใช้ตัวข้างบนไม่ได้ให้ใช้ตัวด้านล่าง for people who unsuccessfully used above code group then use this code group
        try {
            Statement statement = conn.createStatement();
            ResultSet generatedKeys = statement.executeQuery("SELECT last_insert_rowid()");
            if(generatedKeys.next()) {
                return generatedKeys.getInt(1);
            }
        } catch(SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
}
