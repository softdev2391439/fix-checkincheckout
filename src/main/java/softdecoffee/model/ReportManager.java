/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nobpharat
 */
public class ReportManager {

	private int productId;
	private String productName;
	private int productQtySale;
	private double totalIncome;
	private double totalCost;
	private double totalProfit;
	private int materialId;
	private double materialQty;
	private String materialName;
	private double empCost;
	private double utilityCost;
	private double electricCost;
	private double waterCost;
	private double rentalCost;
	private double materialCost;
	private Date date;

	public ReportManager(int productId, String productName, int productQtySale, double totalSales, double totalCost, double totalProfit, int materialId, double materialQty, String materialName, double empPay, double utilityCost, double electricCost, double waterCost, double rentalCost, double materialCost, Date date) {
		this.productId = productId;
		this.productName = productName;
		this.productQtySale = productQtySale;
		this.totalIncome = totalSales;
		this.totalCost = totalCost;
		this.totalProfit = totalProfit;
		this.materialId = materialId;
		this.materialQty = materialQty;
		this.materialName = materialName;
		this.empCost = empPay;
		this.utilityCost = utilityCost;
		this.electricCost = electricCost;
		this.waterCost = waterCost;
		this.rentalCost = rentalCost;
		this.materialCost = materialCost;
		this.date = date;

	}

	public ReportManager() {
		this(-1, "", 0, 0, 0, 0, -1, 0, "", 0, 0, 0, 0, 0, 0, null);
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductQtySale() {
		return productQtySale;
	}

	public void setProductQtySale(int productQtySale) {
		this.productQtySale = productQtySale;
	}

	public double getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(double totalSales) {
		this.totalIncome = totalSales;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public double getTotalProfit() {
		return totalProfit;
	}

	public void setTotalProfit(double totalProfit) {
		this.totalProfit = totalProfit;
	}

	public int getMaterialId() {
		return materialId;
	}

	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}

	public double getMaterialQty() {
		return materialQty;
	}

	public void setMaterialQty(double materialQty) {
		this.materialQty = materialQty;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public double getEmpCost() {
		return empCost;
	}

	public void setEmpCost(double empCost) {
		this.empCost = empCost;
	}

	public double getUtilityCost() {
		return utilityCost;
	}

	public void setUtilityCost(double utilityCost) {
		this.utilityCost = utilityCost;
	}

	public double getElectricCost() {
		return electricCost;
	}

	public void setElectricCost(double electricCost) {
		this.electricCost = electricCost;
	}

	public double getWaterCost() {
		return waterCost;
	}

	public void setWaterCost(double waterCost) {
		this.waterCost = waterCost;
	}

	public double getRentalCost() {
		return rentalCost;
	}

	public void setRentalCost(double rentalCost) {
		this.rentalCost = rentalCost;
	}

	public double getMaterialCost() {
		return materialCost;
	}

	public void setMaterialCost(double materialCost) {
		this.materialCost = materialCost;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ReportManager{" + "productId=" + productId + ", productName=" + productName + ", productQtySale=" + productQtySale + ", totalSales=" + totalIncome + ", totalCost=" + totalCost + ", totalProfit=" + totalProfit + ", materialId=" + materialId + ", materialQty=" + materialQty + ", materialName=" + materialName + ", empCost=" + empCost + ", utilityCost=" + utilityCost + ", electricCost=" + electricCost + ", waterCost=" + waterCost + ", rentalCost=" + rentalCost + ", materialCost=" + materialCost + ", date=" + date + '}';
	}

	public static ReportManager fromRSBestSale(ResultSet rs) {
		ReportManager object = new ReportManager();
		try {
			object.setProductId(rs.getInt("P_ID"));
			object.setProductName(rs.getString("P_NAME"));
			object.setProductQtySale(rs.getInt("P_TOTALQTY"));
		} catch (SQLException ex) {
			Logger.getLogger(ReportManager.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
		return object;
	}

	public static ReportManager fromRSMoney(ResultSet rs) {
		ReportManager object = new ReportManager();
		try {
			object.setTotalIncome(rs.getDouble("TOTAL_INCOME"));
			object.setTotalCost(rs.getDouble("TOTAL_COST"));
			object.setTotalProfit(rs.getDouble("TOTAL_PROFIT"));
			object.setEmpCost(rs.getDouble("EMP_PAY"));
			object.setUtilityCost(rs.getDouble("UC_PAY"));
			object.setElectricCost(rs.getDouble("ELEC_PAY"));
			object.setWaterCost(rs.getDouble("WATER_PAY"));
			object.setRentalCost(rs.getDouble("RENT_PAY"));
			object.setMaterialCost(rs.getDouble("MAT_PAY"));
			String dateStr = rs.getString("DATE");
			if (dateStr != null) {
				try {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
					LocalDate localDate = LocalDate.parse(dateStr, formatter);
					object.setDate(java.sql.Date.valueOf(localDate));
				} catch (DateTimeParseException ex) {
					Logger.getLogger(ReportManager.class.getName()).log(Level.SEVERE, null, ex);
					object.setDate(null);
				}
			} else {
				object.setDate(null);
			}
		} catch (SQLException ex) {
			Logger.getLogger(ReportManager.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
		return object;
	}

	public static ReportManager fromRSMaterial(ResultSet rs) {
		ReportManager object = new ReportManager();
		try {
			object.setMaterialId(rs.getInt("M_ID"));
			object.setMaterialQty(rs.getDouble("M_QTY"));
			object.setMaterialName(rs.getString("M_NAME"));
		} catch (SQLException ex) {
			Logger.getLogger(ReportManager.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
		return object;
	}


}
