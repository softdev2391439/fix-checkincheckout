/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author natta
 */
public class Customer {

    private int Id;
    private String Fname;
    private String Lname;
    private String Email;
    private String Tel;
    private String Address;
    private String Gender;
    private Date Indate;
    private double CUST_P_VALUE;
    private double CUST_P_RATE;

    public Customer(int Id, String Fname, String Lname, String Email, String Tel, String Address, String Gender, Date Indate, float CUST_P_VALUE, float CUST_P_RATE) {
        this.Id = Id;
        this.Fname = Fname;
        this.Lname = Lname;
        this.Email = Email;
        this.Tel = Tel;
        this.Address = Address;
        this.Gender = Gender;
        this.Indate = Indate;
        this.CUST_P_VALUE = CUST_P_VALUE;
        this.CUST_P_RATE = CUST_P_RATE;
    }

    public Customer(String Fname, String Lname, String Email, String Tel, String Address, String Gender, Date Indate, float CUST_P_VALUE, float CUST_P_RATE) {
        this.Id = -1;
        this.Fname = Fname;
        this.Lname = Lname;
        this.Email = Email;
        this.Tel = Tel;
        this.Address = Address;
        this.Gender = Gender;
        this.Indate = Indate;
        this.CUST_P_VALUE = CUST_P_VALUE;
        this.CUST_P_RATE = CUST_P_RATE;
    }

    public Customer(String Fname, String Lname, String Email, String Tel, String Address, String Gender) {
        this.Id = -1;
        this.Fname = Fname;
        this.Lname = Lname;
        this.Email = Email;
        this.Tel = Tel;
        this.Address = Address;
        this.Gender = Gender;
        this.Indate = null;
        this.CUST_P_VALUE = 0;
        this.CUST_P_RATE = 10;
    }

    public Customer() {
        this.Id = -1;
        this.Fname = "";
        this.Lname = "";
        this.Email = "";
        this.Tel = "";
        this.Address = "";
        this.Gender = "";
        this.Indate = null;
        this.CUST_P_VALUE = 0;
        this.CUST_P_RATE = 10;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String Fname) {
        this.Fname = Fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String Lname) {
        this.Lname = Lname;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public Date getIndate() {
        return Indate;
    }

    public void setIndate(Date Indate) {
        this.Indate = Indate;
    }

    public double getCUST_P_VALUE() {
        return CUST_P_VALUE;
    }

    public void setCUST_P_VALUE(double CUST_P_VALUE) {
        this.CUST_P_VALUE = CUST_P_VALUE;
    }

    public double getCUST_P_RATE() {
        return CUST_P_RATE;
    }

    public void setCUST_P_RATE(double CUST_P_RATE) {
        this.CUST_P_RATE = CUST_P_RATE;
    }

    @Override
    public String toString() {
        return "CustomerModel{" + "Id=" + Id + ", Fname=" + Fname + ", Lname=" + Lname + ", Email=" + Email + ", Tel=" + Tel + ", Address=" + Address + ", Gender=" + Gender + ", Indate=" + Indate + ", CUST_P_VALUE=" + CUST_P_VALUE + ", CUST_P_RATE=" + CUST_P_RATE + '}';
    }

    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setId(rs.getInt("CUS_CODE"));
            customer.setFname(rs.getString("CUS_FNAME"));
            customer.setLname(rs.getString("CUS_LNAME"));
            customer.setEmail(rs.getString("CUS_EMAIL"));
            customer.setTel(rs.getString("CUS_TEL"));
            customer.setAddress(rs.getString("CUS_ADD"));
            customer.setGender(rs.getString("CUS_GEN"));
            String dateStr = rs.getString("CUS_INDATE");
			if (dateStr != null) {
				try {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					java.util.Date parsedDate = dateFormat.parse(dateStr);
					customer.setIndate(new java.sql.Date(parsedDate.getTime()));
				} catch (ParseException ex) {
					Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
					customer.setIndate(null);
				}
			} else {
				customer.setIndate(null);
			}
            customer.setCUST_P_VALUE(rs.getDouble("CUS_P_VALUE"));
            customer.setCUST_P_RATE(rs.getDouble("CUS_P_RATE"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }
}

