/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author basba
 */
public class PromotionDetail {

    private int id;
    private int rcId;
    private int proId;
    private Double discount;

    public PromotionDetail(int id, int rcId, int proId, Double discount) {
        this.id = id;
        this.rcId = rcId;
        this.proId = proId;
        this.discount = discount;
    }

    public PromotionDetail(int rcId, int proId, Double discount) {
        this.id = -1;
        this.rcId = rcId;
        this.proId = proId;
        this.discount = discount;
    }

    public PromotionDetail() {
        this.id = -1;
        this.rcId = -1;
        this.proId = -1;
        this.discount = 0.0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRcId() {
        return rcId;
    }

    public void setRcId(int rcId) {
        this.rcId = rcId;
    }

    public int getProId() {
        return proId;
    }

    public void setProId(int proId) {
        this.proId = proId;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "PromotionDetail{" + "id=" + id + ", rcId=" + rcId + ", proId=" + proId + ", discount=" + discount + '}';
    }

    public static PromotionDetail fromRS(ResultSet rs) {
        PromotionDetail promotionDetail = new PromotionDetail();
        try {
            promotionDetail.setId(rs.getInt("PRDE_CODE"));
            promotionDetail.setRcId(rs.getInt("RC_CODE"));
            promotionDetail.setProId(rs.getInt("PRO_CODE"));
            promotionDetail.setDiscount(rs.getDouble("PRDE_DISCOUNT"));

        } catch (SQLException ex) {
            Logger.getLogger(PromotionDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotionDetail;
    }

//    public PromotionDetail get(int rowIndex) {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }
}
