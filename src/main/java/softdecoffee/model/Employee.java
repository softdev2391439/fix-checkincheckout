/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Employee {

	private static String String;
	private int id;
	private String fname;
	private String lname;
	private String email;
	private String tel;
	private Date indate;
	private String address;
	private String position;
	private String status;
	private String password;
	private String username;
	private String login;

	public Employee(int id, String fname, String lname, String email, String tel, Date indate, String address, String position, String status, String password, String login) {
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.tel = tel;
		this.address = address;
		this.position = position;
		this.status = status;
		this.password = password;
		this.login = login;
	}

	public Employee(String fname, String lname, String email, String tel, Date indate, String address, String position, String status, String password, String login) {
		this.id = -1;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.tel = tel;
		this.indate = null;
		this.address = address;
		this.position = position;
		this.status = status;
		this.password = password;
		this.login = login;
	}

	public Employee(String fname, String lname, String email, String tel, String address, String position, String status, String password, String login, String username) {
		this.id = -1;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.tel = tel;
		this.indate = null;
		this.address = address;
		this.position = position;
		this.status = status;
		this.password = password;
		this.login = login;
		this.username = username;
	}
        
        public Employee(String fname, String lname) {
		this.id = -1;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.tel = tel;
		this.indate = null;
		this.address = address;
		this.position = position;
		this.status = status;
		this.password = password;
		this.login = login;
		this.username = username;
	}

	public Employee() {
		this.id = -1;
		this.fname = "";
		this.lname = "";
		this.email = "";
		this.tel = "";
		this.address = "";
		this.position = "";
		this.status = "";
		this.password = "";
		this.login = "";
		this.username = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Date getIndate() {
		return indate;
	}

	public void setIndate(Date indate) {
		this.indate = indate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return "Employee{" + "id=" + id + ", fname=" + fname + ", lname=" + lname + ", email=" + email + ", tel=" + tel + ", indate=" + indate + ", address=" + address + ", position=" + position + ", status=" + status + ", password=" + password + '}';
	}

	public static Employee fromRS(ResultSet rs) {
		Employee employee = new Employee();
		try {
			employee.setId(rs.getInt("EMP_CODE"));
			employee.setFname(rs.getString("EMP_FNAME"));
			employee.setLname(rs.getString("EMP_LNAME"));
			employee.setEmail(rs.getString("EMP_EMAIL"));
			employee.setTel(rs.getString("EMP_TEL"));
			String dateStr = rs.getString("EMP_INDATE");
			if (dateStr != null) {
				try {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					java.util.Date parsedDate = dateFormat.parse(dateStr);
					employee.setIndate(new java.sql.Date(parsedDate.getTime()));
				} catch (ParseException ex) {
					Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
					employee.setIndate(null);
				}
			} else {
				employee.setIndate(null);
			}

			employee.setAddress(rs.getString("EMP_ADDRESS"));
			employee.setPosition(rs.getString("EMP_RANK"));
			employee.setStatus(rs.getString("EMP_STATUS"));
			employee.setPassword(rs.getString("EMP_PASS"));
			employee.setLogin(rs.getString("EMP_USERNAME"));
		} catch (SQLException ex) {
			Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
		return employee;
	}
        
    public String getFullName() {
        return fname + " " + lname;
    }

    

}
