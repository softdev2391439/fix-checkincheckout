/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author natta
 */
public class TopTenSalesReport {
    private int id;
    private String name;
    private int totalQuatity;
    private  float totalPrice;

    public TopTenSalesReport(int id, String name, int totalQuatity, float totalPrice) {
        this.id = id;
        this.name = name;
        this.totalQuatity = totalQuatity;
        this.totalPrice = totalPrice;
    }

    public TopTenSalesReport(String name, int totalQuatity, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.totalQuatity = totalQuatity;
        this.totalPrice = totalPrice;
    }
    public TopTenSalesReport(){
        this(-1,"",0,0);
        
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalQuatity() {
        return totalQuatity;
    }

    public void setTotalQuatity(int totalQuatity) {
        this.totalQuatity = totalQuatity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ArtistReport{" + "id=" + id + ", name=" + name + ", totalQuatity=" + totalQuatity + ", totalPrice=" + totalPrice + '}';
    }
    
    
    
    public static TopTenSalesReport fromRS(ResultSet rs) {
        TopTenSalesReport obj = new TopTenSalesReport();
        try {
            obj.setId(rs.getInt("PROD_CODE"));
                obj.setName(rs.getString("PROD_NAME"));
            obj.setTotalQuatity(rs.getInt("TotalQuantity"));
            obj.setTotalPrice(rs.getFloat("TotalPrice"));
        } catch (SQLException ex) {
            Logger.getLogger(TopTenSalesReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
