/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import com.sun.nio.sctp.SctpChannel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nobpharat
 */
public class StockBillDetail {

	private int id;
	private int biiId;
	private double amount;
	private double price;
	private double totalPrrice;
	private int materrialId;

	public StockBillDetail(int id, int biiId, double amount, double price, double totalPrrice, int materrialId) {
		this.id = id;
		this.biiId = biiId;
		this.amount = amount;
		this.price = price;
		this.totalPrrice = totalPrrice;
		this.materrialId = materrialId;
	}

	public StockBillDetail(int biiId, double amount, double price, double totalPrrice, int materrialId) {
		this.id = -1;
		this.biiId = biiId;
		this.amount = amount;
		this.price = price;
		this.totalPrrice = totalPrrice;
		this.materrialId = materrialId;
	}

	public StockBillDetail() {
		this.id = -1;
		this.biiId = 0;
		this.amount = 0;
		this.price = 0;
		this.totalPrrice = 0;
		this.materrialId = 0;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBiiId() {
		return biiId;
	}

	public void setBiiId(int biiId) {
		this.biiId = biiId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTotalPrrice() {
		return totalPrrice;
	}

	public void setTotalPrrice(double totalPrrice) {
		this.totalPrrice = totalPrrice;
	}

	public int getMaterrialId() {
		return materrialId;
	}

	public void setMaterrialId(int materrialId) {
		this.materrialId = materrialId;
	}

	@Override
	public String toString() {
		return "StockBillDetail{" + "id=" + id + ", biiId=" + biiId + ", amount=" + amount + ", price=" + price + ", totalPrrice=" + totalPrrice + ", materrialId=" + materrialId + '}';
	}

	public static StockBillDetail fromRS(ResultSet rs) {
		StockBillDetail stockBillDetail = new StockBillDetail();
		try {
			stockBillDetail.setId(rs.getInt("BD_CODE"));
			stockBillDetail.setBiiId(rs.getInt("BILL_CODE"));
			stockBillDetail.setAmount(rs.getDouble("BD_AMOUNT"));
			stockBillDetail.setPrice(rs.getDouble("BD_PRICE_PER_UNIT"));
			stockBillDetail.setTotalPrrice(rs.getDouble("BD_PRICE_TOTAL"));
			stockBillDetail.setMaterrialId(rs.getInt("M_CODE"));
		} catch (SQLException ex) {
			Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
		return stockBillDetail;
	}

}
