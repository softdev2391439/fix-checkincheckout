/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Receipt {

    private int id;
    private int empId;
    private int cusId;
    private float receiptTotal;
    private Date receiptDate;
    private float receiptPayment;
    private float receiptChange;
    private float receiptNetTotal;
    private float receiptDiscount;
    private int receiptTotalQty;
    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList<ReceiptDetail>();
    private ReceiptDetail receiptDetail;
    public double calculateTotal;
    public Object getReceiptDetails;

    public Receipt(int empId, int cusId, float receiptTotal, Date receiptDate, float receiptPayment, float receiptChange, float receiptNetTotal, float receiptDiscount, int receiptTotalQty) {
        this.id = -1;
        this.empId = -1;
        this.cusId = cusId;
        this.receiptTotal = receiptTotal;
        this.receiptDate = receiptDate;
        this.receiptPayment = receiptPayment;
        this.receiptChange = receiptChange;
        this.receiptNetTotal = receiptNetTotal;
        this.receiptDiscount = receiptDiscount;
        this.receiptTotalQty = receiptTotalQty;
    }

    public Receipt(float receiptTotal, float receiptPayment, int receiptTotalQty, int empId, int cusId) {
        this.id = -1;
        this.empId = -1;
        this.cusId = -1;
        this.receiptTotal = receiptTotal;
        this.receiptPayment = receiptPayment;
        this.receiptChange = 0;
        this.receiptNetTotal = 0;
        this.receiptDiscount = 0;
        this.receiptTotalQty = receiptTotalQty;
    }

    public Receipt(float receiptPayment, int empId, int cusId) {
        this.id = -1;
        this.receiptTotal = 0;
        this.receiptPayment = receiptPayment;
        this.receiptChange = 0;
        this.receiptNetTotal = 0;
        this.receiptDiscount = 0;
        this.receiptTotalQty = 0;
    }

    public Receipt() {
        this.id = -1;
        this.empId = -1;
        this.cusId = -1;
        this.receiptTotal = 0;
        this.receiptDate = null;
        this.receiptPayment = 0;
        this.receiptChange = 0;
        this.receiptNetTotal = 0;
        this.receiptDiscount = 0;
        this.receiptTotalQty = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public float getReceiptTotal() {
        return receiptTotal;
    }

    public void setReceiptTotal(float receiptTotal) {
        this.receiptTotal = receiptTotal;
        calculateTotal();
    }

    public Date getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(Date receiptDate) {
        this.receiptDate = receiptDate;
    }

    public float getReceiptPayment() {
        return receiptPayment;
    }

    public void setReceiptPayment(float receiptPayment) {
        this.receiptPayment = receiptPayment;
    }

    public float getReceiptChange() {
        return receiptChange;
    }

    public void setReceiptChange(float receiptChange) {
        this.receiptChange = receiptChange;
    }

    public float getReceiptNetTotal() {
        return receiptNetTotal;
    }

    public void setReceiptNetTotal(float receiptNetTotal) {
        this.receiptNetTotal = receiptNetTotal;
    }

    public float getReceiptDiscount() {
        return receiptDiscount;
    }

    public void setReceiptDiscount(float receiptDiscount) {
        this.receiptDiscount = receiptDiscount;
    }

    public int getReceiptTotalQty() {
        return receiptTotalQty;
    }

    public void setReceiptTotalQty(int receiptTotalQty) {
        this.receiptTotalQty = receiptTotalQty;
    }

    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

    public void setReceiptDetails(ArrayList<ReceiptDetail> receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

    public void addReceiptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.add(receiptDetail);
        calculateTotal();
    }

    public void addReceiptDetail(Product product, int qty) {
        ReceiptDetail rd = new ReceiptDetail(product.getId(), product.getName(), product.getPrice(), qty, qty * product.getPrice(), -1);
        receiptDetails.add(rd);
        calculateTotal();
    }

    public void delReceiptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.remove(receiptDetail);
        calculateTotal();
    }

    public void calculateTotal() {

        float total = 0;
        int totalQty = 0;

        for (ReceiptDetail rd : receiptDetails) {
            float detailTotal = rd.getReceiptDetailQty() * rd.getReceiptDetailUnitPrice();
            total += detailTotal;
            totalQty += rd.getReceiptDetailQty();
        }
        
        float discount = receiptDiscount;
        float totalNet = total - discount;
        this.receiptTotal = total;
        this.receiptTotalQty = totalQty;
        this.receiptNetTotal = totalNet;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", empId=" + empId + ", cusId=" + cusId + ", receiptTotal=" + receiptTotal + ", receiptDate=" + receiptDate + ", receiptPayment=" + receiptPayment + ", receiptChange=" + receiptChange + ", receiptNetTotal=" + receiptNetTotal + ", receiptDiscount=" + receiptDiscount + ", receiptTotalQty=" + receiptTotalQty + ", receiptDetails=" + receiptDetails + ", receiptDetail=" + receiptDetail + '}';
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("RE_CODE"));
            receipt.setEmpId(rs.getInt("EMP_CODE"));
            receipt.setCusId(rs.getInt("CUS_CODE"));
            receipt.setReceiptTotal(rs.getFloat("RE_TOTAL"));
            receipt.setReceiptDate(rs.getDate("RE_DATE"));
            receipt.setReceiptPayment(rs.getFloat("RE_PAYMENT"));
            receipt.setReceiptChange(rs.getFloat("RE_CHANGE"));
            receipt.setReceiptNetTotal(rs.getFloat("RE_NET_TOTAL"));
            receipt.setReceiptDiscount(rs.getFloat("RE_DISCOUNT"));
            receipt.setReceiptTotalQty(rs.getInt("RE_TOTAL_QTY"));
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return receipt;
    }
}
