/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Puri
 */
public class StockBill {
    private int id;
    private double totalPrice;
    private int totalQty;
    private Date dateTime;
    private int employeeId;
    private double billPay;
    private int vendorId;
    private Vendor vendor;
    private ArrayList<StockBillDetail> stockBillDetails = new ArrayList();

    public ArrayList<StockBillDetail> getStockBillDetails() {
        return stockBillDetails;
    }

    public void setStockBillDetails(ArrayList<StockBillDetail> stockBillDetails) {
        this.stockBillDetails = stockBillDetails;
    }
    public void addStockBillDetail(StockBillDetail stockBillDetail) {
        StockBillDetail pd = new StockBillDetail(stockBillDetail.getId(), stockBillDetail.getBiiId(), stockBillDetail.getAmount(), stockBillDetail.getPrice(),stockBillDetail.getTotalPrrice(),stockBillDetail.getMaterrialId());
        stockBillDetails.add(pd);
    }
    
    public void clearStockBillDetails() {
        stockBillDetails.clear();
    }
  
    public void delStockBillDetail(StockBillDetail stockBillDetail){
        stockBillDetails.remove(stockBillDetail);
    }

    public StockBill(int id, double totalPrice, int totalQty, Date dateTime, int employeeId, double billPay, int vendorId) {
        this.id = id;
        this.totalPrice = totalPrice;
        this.totalQty = totalQty;
        this.dateTime = dateTime;
        this.employeeId = employeeId;
        this.billPay = billPay;
        this.vendorId = vendorId;
    }
    
    public StockBill(double totalPrice, int totalQty, Date dateTime, int employeeId, double billPay, int vendorId) {
        this.id = -1;
        this.totalPrice = totalPrice;
        this.totalQty = totalQty;
        this.dateTime = dateTime;
        this.employeeId = employeeId;
        this.billPay = billPay;
        this.vendorId = vendorId;
    }
    
    public StockBill(double totalPrice, int totalQty, double billPay, int vendorId) {
        this.id = -1;
        this.totalPrice = totalPrice;
        this.totalQty = totalQty;
        this.billPay = billPay;
        this.vendorId = vendorId;
    }
    
    public StockBill() {
        this.id = -1;
        this.totalPrice = 0.0;
        this.totalQty = 0;
        this.dateTime = null;
        this.employeeId = 0;
        this.billPay = 0.0;
        this.vendorId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public double getBillPay() {
        return billPay;
    }

    public void setBillPay(double billPay) {
        this.billPay = billPay;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public StockBill(Vendor vendor) {
        this.vendor = vendor;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Override
    public String toString() {
        return "StockBill{" + "id=" + id + ", totalPrice=" + totalPrice + ", totalQty=" + totalQty + ", dateTime=" + dateTime + ", employeeId=" + employeeId + ", billPay=" + billPay + ", vendorId=" + vendorId + ", vendor=" + vendor + '}';
    }

    
    
    
    public static StockBill fromRS(ResultSet rs) {
        StockBill stockBill = new StockBill();
        try {
            stockBill.setId(rs.getInt("BILL_CODE"));
            stockBill.setTotalPrice(rs.getInt("BILL_TOTAL_PRICE"));
            stockBill.setTotalQty(rs.getInt("BILL_TOTAL_QTY"));
            stockBill.setDateTime(rs.getDate("BILL_DATETIME"));
            stockBill.setEmployeeId(rs.getInt("EMP_CODE"));
            stockBill.setBillPay(rs.getDouble("BILL_PAY"));
            stockBill.setVendorId(rs.getInt("V_CODE"));
            
        } catch (SQLException ex) {
            Logger.getLogger(StockBill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockBill;
    }
    
}
