/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ten
 */
public class Product {
    private int id;
    private String name;
    private float price;
    private String category;
    private String subCategory;
    private String size;
    private String sweetLevel;
    private String nameCategory;

    public Product(int id, String name, float price, String category, String subCategory, String size,String sweetLevel,String nameCategory) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
        this.subCategory = subCategory;
        this.size = size;
        this.sweetLevel = sweetLevel;
        this.nameCategory= nameCategory;
    }

    public Product(String name, float price, String category, String subCategory, String size,String sweetLevel,String nameCategory) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.category = category;
        this.subCategory = subCategory;
        this.size = size;
        this.sweetLevel = sweetLevel;
        this.nameCategory= nameCategory;
    }
    
    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.category = "";
        this.subCategory = "";
        this.size = "";
        this.sweetLevel  = "";
        this.nameCategory = "";
    }

    public Product(int id, String name, float price, String category, String size,String sweetLevel,String nameCategory) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.category = category;
        this.subCategory = subCategory;
        this.size = size;
        this.sweetLevel = sweetLevel;
        this.nameCategory = nameCategory;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSweetLevel() {
        return sweetLevel;
    }

    public void setSweetLevel(String sweetLevel) {
        this.sweetLevel = sweetLevel;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", category=" + category + ", subCategory=" + subCategory + ", size=" + size + ", sweetLevel=" + sweetLevel + ", nameCategory=" + nameCategory + "}";
    }

    
    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("PROD_CODE"));
            product.setName(rs.getString("PROD_NAME"));
            product.setPrice(rs.getFloat("PROD_PRICE"));
            product.setCategory(rs.getString("PROD_CATEGORY"));
            product.setSubCategory(rs.getString("PROD_SUB_CATEGORY"));
            product.setSize(rs.getString("PROD_SIZE"));
            product.setSweetLevel(rs.getString("PROD_SWEET_LEVEL"));
            product.setNameCategory(rs.getString("CAT_NAME"));
          
            
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }    
}
 